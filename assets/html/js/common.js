function getWidth() {
	xWidth = null;
	if (window.screen != null)
		xWidth = window.screen.availWidth;

	/*
	if (window.innerWidth != null)
		xWidth = window.innerWidth;

	if (document.body != null)
		xWidth = document.body.clientWidth;*/

	return xWidth;
}

function getHeight() {
	xHeight = null;
	if (window.screen != null)
		xHeight = window.screen.availHeight;

	/*
	if (window.innerHeight != null)
		xHeight = window.innerHeight;

	if (document.body != null)
		xHeight = document.body.clientHeight;*/

	return xHeight;
}

function initFlash() {
	var flashvars = {};
	var params = {
		allowScriptAccess : "always",
		allowFullScreen : "true"
	};
	var attributes = {
		id : "sanmuflash",
		play : "true",
		loop : "false"
	};

	/* Play with file name replacing */
	var fWidth = getWidth()-10;
	var fHeight = getHeight()-10;
	if (fWidth < fHeight) {
		swfobject.embedSWF(courseFile, "flashcontent", fHeight, fWidth,
				"9.0.0", "expressInstall.swf", flashvars, params, attributes);
	} else {
		swfobject.embedSWF(courseFile, "flashcontent", fWidth, fHeight,
				"9.0.0", "expressInstall.swf", flashvars, params, attributes);
	}

	// alert("window.screen.width=" + window.screen.width + ",
	// window.screen.height=" + window.screen.height);

	/* Play with file name combined in URL */
	// var urlParams = urlObject().parameters;
	// var fileName = urlParams['file'];
	// swfobject.embedSWF(fileName, "flashcontent", window.screen.width-100,
	// window.screen.height-80, "9.0.0", "expressInstall.swf", flashvars,
	// params, attributes);
	// swfobject.embedSWF("a.swf", "flashcontent", "300", "120", "9.0.0");
}

function puaseFlash() {
	// alert("pause");
	swfobject.getObjectById('sanmuflash').StopPlay();
}

function playFlash() {
	// alert("play");
	swfobject.getObjectById('sanmuflash').Play();
}

function rewindFlash() {
	// alert("Rewind");
	swfobject.getObjectById('sanmuflash').Rewind();
}

function setFocusFlash() {
	swfobject.getObjectById('sanmuflash').focus();
}