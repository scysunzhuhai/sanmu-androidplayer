/* ID: $Id: MainActivity.java 78 2013-10-07 08:30:17Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-10-07 16:30:17 +0800 (周一, 07 十月 2013) $
 * 
 * @version $Rev: 78 $
 */
package com.cnmpr.mprplayer;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.cnmpr.mprplayer.R;
import com.cnmpr.mprplayer.constant.FolderDirection;
import com.cnmpr.mprplayer.constant.MyConstants;
import com.cnmpr.mprplayer.core.BaseActivity;
import com.cnmpr.mprplayer.listener.CheckedChangedListener;
import com.cnmpr.mprplayer.listener.ConfirmDeleteDialogListener;
import com.cnmpr.mprplayer.listener.ConfirmMoveDialogListener;
import com.cnmpr.mprplayer.listener.ConfirmPlayLatestDialogListener;
import com.cnmpr.mprplayer.listener.FileClickListener;
import com.cnmpr.mprplayer.listener.FolderEntryListener;
import com.cnmpr.mprplayer.listener.LongClickListener;
import com.cnmpr.mprplayer.util.MyFilenameFilter;
import com.cnmpr.mprplayer.util.MyFolderFilter;
import com.cnmpr.mprplayer.util.SharedPreferencesUtils;
import com.cnmpr.mprplayer.util.UIHelper;

import android.net.Uri;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends BaseActivity
{
	public static final int MAX_NUM_A_ROW = 2;

	/*Max number of histories that files be moved or deleted*/
	public static final int MAX_NUM_OF_FILE_HISTORY = 5;
	
	/*Keep the current path*/
	private String currPath = null;
	private String theLatestFilePath = null;
	
	private OnClickListener fileClickListener = new FileClickListener(this);
	private OnClickListener folderEntryListener = new FolderEntryListener(this);
	private LongClickListener fileLongClickListener = new LongClickListener(this, true);
	private LongClickListener folderLongClickListener = new LongClickListener(this, false);
	private CheckedChangedListener checkedChangedListener = new CheckedChangedListener(this);
	
	private ConfirmDeleteDialogListener confirmDeleteDialogListener = new ConfirmDeleteDialogListener(this);
	private ConfirmMoveDialogListener confirmMoveDialogListener = new ConfirmMoveDialogListener(this);

	private FilenameFilter jsmdFilter = new MyFilenameFilter(); 
	private MyFolderFilter folderFilter = new MyFolderFilter();
	
	private Button btnBack;
	private TextView txtCurrPath;

	private MenuItem menuRefresh;
	private MenuItem menuMove;
	private MenuItem menuMoveCancel;
	private MenuItem menuPaste;
	private MenuItem menuDelete;
	private MenuItem menuExit;
	private MenuItem menuAbout;
	
	private AlertDialog altDlgDelete;
	
	private AlertDialog altDlgMove;
	private boolean isMoveFiles = false;
	private String moveFromPath;
	
	//private Spinner foldersSpinner;
	
	private List<String> currSelectedList = new ArrayList<String>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

//		TableLayout mainTable = (TableLayout) this.findViewById(R.id.mainTable);

		/***** Initialize component *****/		
		//Navigation path
		txtCurrPath = (TextView)this.findViewById(R.id.txtCurrPath);
		txtCurrPath.setText("/");
		
		//Back button
		btnBack = (Button)this.findViewById(R.id.btnBack);
		btnBack.setOnClickListener(folderEntryListener);
		btnBack.setEnabled(false);
//		this.registerForContextMenu(btnBack);		

		checkRootPath();
		
		/*
		foldersSpinner = (Spinner)this.findViewById(R.id.lstFolders);
		foldersSpinner.setOnFocusChangeListener(new OnFocusChangeListener(){
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if(!hasFocus){
					v.setVisibility(View.INVISIBLE);
				}
			}
		});*/
		
		String initialPath = MyConstants.FLASH_ROOT;
		
		theLatestFilePath = SharedPreferencesUtils.readData(this, MyConstants.KEY_OF_LATEST_FILE);

		//Check if has the latest played file
		if(StringUtils.isNotBlank(theLatestFilePath)){
			String msg = this.getString(R.string.msg_alert_replay_latest, this.beautifyPathString(theLatestFilePath));
			AlertDialog altDlgReplay = this.createAlertDialog(this.getString(R.string.title_alert_replay_latest), msg, new ConfirmPlayLatestDialogListener(this));
			altDlgReplay.show();
		}
		
		//Folder list
		updateCurrentPath(initialPath, FolderDirection.BACKWARD);
		refreshDiretory();
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	public void onResume()
	{
		super.onResume();
		
		enableMoving(this.isMoveFiles, false);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_main, menu);
		
		menuRefresh = (MenuItem)menu.findItem(R.id.menu_refresh);
		menuMove = (MenuItem)menu.findItem(R.id.menu_move);
		menuMoveCancel = (MenuItem)menu.findItem(R.id.menu_move_cancel);
		menuDelete = (MenuItem)menu.findItem(R.id.menu_delete);
		menuPaste = (MenuItem)menu.findItem(R.id.menu_paste);	
		menuExit = (MenuItem)menu.findItem(R.id.menu_exit);
		menuAbout = (MenuItem)menu.findItem(R.id.menu_about);
		
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId()){
			case R.id.menu_refresh:
				this.refreshDiretory();
				break;
			case R.id.menu_move:
				enableMoving(true, false);
				moveFromPath = this.currPath;
				
				/*
				//Show Spinner of folder list
				foldersSpinner.setVisibility(View.VISIBLE);
				List<String> folderList = this.findAllFolders(MyConstants.FLASH_ROOT);
				
				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
						android.R.layout.simple_spinner_item, folderList);
				dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				foldersSpinner.setAdapter(dataAdapter);
				
				foldersSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){

					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
					{
						//Spinner spn = (Spinner)view;
						String target = parent.getItemAtPosition(position).toString();

						if(altDlgMove==null){
							String msg = getString(R.string.msg_alert_move)
								+ IOUtils.LINE_SEPARATOR 
								+ "目标路径:" + target;
							altDlgMove = createAlertDialog(
								getString(R.string.title_alert_move),
								msg,
								confirmMoveDialogListener);
						}
						altDlgMove.show();
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent)
					{
						Log.d("NothingSelected", "NothingSelected");
					}
				});
				
				foldersSpinner.requestFocus();
				foldersSpinner.performClick();
				*/
				
				break;
			case R.id.menu_move_cancel:
				isMoveFiles = false;
				enableMoving(isMoveFiles, true);
				moveFromPath = "";	
				this.toastShortMessage(getString(R.string.msg_move_cancelled));
				break;
			case R.id.menu_paste:
				if(moveFromPath.equals(currPath)){
					this.toastWarningMessage(getString(R.string.msg_alert_samefolder));
				}
				else{
					String msg = getString(R.string.msg_alert_move)
						+ IOUtils.LINE_SEPARATOR 
						+ getString(R.string.msg_after_move_target) + currPath;
					if(altDlgMove==null){
						altDlgMove = createAlertDialog(
							getString(R.string.title_alert_move),
							msg,
							confirmMoveDialogListener);
					}
					altDlgMove.setMessage(msg);
					altDlgMove.show();					
				}
				break;
			case R.id.menu_delete:
				if(altDlgDelete==null){
					altDlgDelete = this.createAlertDialog(
						getString(R.string.title_alert_delete),
						getString(R.string.msg_alert_delete),
						confirmDeleteDialogListener
						);
				}
				altDlgDelete.show();
				
				break;
			case R.id.menu_exit:
				confirmExit(true);
				break;
			case R.id.menu_about:
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.cnmpr.com"));
				startActivity(browserIntent);
				break;
			default:
				break;
		}
		return true;
	}
	
	/***
	 * Update status of all of menu to cater for isMoving or not
	 * @param isMoving
	 */
	public void enableMoving(boolean isMoving, boolean forceRefresh)
	{
		this.isMoveFiles = isMoving;
		
		//Maybe not initialize
		if(menuMoveCancel!=null){
			menuMoveCancel.setVisible(isMoving);
			menuPaste.setVisible(isMoving);
			
			menuRefresh.setEnabled(!isMoving);
			
			if(!isMoving){
				this.currSelectedList.clear();
			}
			if(forceRefresh){
				this.refreshDiretory();
			}
			
			menuDelete.setEnabled(false);
			menuMove.setEnabled(false);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);
		
		menu.setHeaderTitle("可选操作");
		menu.add(0, 1, 1, "播放");
		menu.add(0, 2, 2, "删除");
		menu.add(0, 3, 3, "移动");
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override 
    public boolean onKeyDown(int keyCode, KeyEvent event) { 
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
        	//If current path is root, return true, else return to parent path
        	if(!isRootPath()){
        		this.updateCurrentPath(getParentPath(), FolderDirection.BACKWARD);
        		refreshDiretory();
                return false; 
        	}
        	else{
        		this.confirmExit(true);
                return false; 
        	}
        } 
        super.onKeyDown(keyCode, event);
        return true; 
    }
	
	/***
	 * Check the sanmu root and flash root existing, if not, create them
	 */
	private void checkRootPath()
	{
		boolean isCreateSucc = false;
		File flashRoot = new File(MyConstants.FLASH_ROOT);
		if(!flashRoot.exists()){
			//Check parent
			File sanmuRoot = flashRoot.getParentFile();
			if(!sanmuRoot.exists()){
				isCreateSucc = sanmuRoot.mkdir();
				debug("Create sanmu root folder success? " + isCreateSucc);
			}
			isCreateSucc = flashRoot.mkdir();
			debug("Create flash folder success? " + isCreateSucc);
		}
	}
	
	/***
	 * Update the current path with given folder and direction
	 * @param currFolder
	 * @param direction
	 */
	public void updateCurrentPath(String currFolder, FolderDirection direction )
	{
		if (currPath == null || currPath.trim().length() == 0) {
			currPath = MyConstants.FLASH_ROOT;
		}
		switch (direction){
			case FORWARD:
				currPath = currPath + MyConstants.FILE_SEP + currFolder;
				break;
			case BACKWARD:
				currPath = currFolder;
				break;
			default:
				break;
		}
		
		String pathStr = beautifyPathString(this.currPath); 
		txtCurrPath.setText(pathStr);
		
		//If root, disable Back button
		if(isRootPath()){
			enableBackButton(false);
		}
		else{
			enableBackButton(true);
		}
	}

	/***
	 * Hide the abstract path of root but only show the rest
	 * @return
	 */
	private String beautifyPathString(String filePath)
	{
		File f = new File(filePath);
		int index = filePath.indexOf(MyConstants.FLASH_ROOT);
		String pathStr = filePath.substring(index + MyConstants.FLASH_ROOT.length());
		if(f.isDirectory()){
			pathStr += MyConstants.FILE_SEP;
		}
		else{
			pathStr = pathStr.substring(0, pathStr.lastIndexOf("."));
		}
		return pathStr;
	}

	/***
	 * Refresh course list under the current folder
	 */
	public void refreshDiretory()
	{
		TableLayout mainTable = (TableLayout) this.findViewById(R.id.mainTable);

		// 1.clean current table
		int childRowNum = mainTable.getChildCount();
		List<TableRow> childRows = new ArrayList<TableRow>();
		for (int i = 0; i < childRowNum; i++) {
			View v = mainTable.getChildAt(i);
			if (v instanceof TableRow) {
				TableRow row = (TableRow) v;
				if (v.getId() != R.id.tableRow1) {
					childRows.add(row);
				}
			}
		}
		for (TableRow row : childRows) {
			row.setVisibility(View.INVISIBLE);
			row.removeAllViews();
			row.setEnabled(false);
			row = null;
		}

		DisplayMetrics metrics = new DisplayMetrics();
		getWindow().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int dpi = metrics.densityDpi;
		
		// 2.get current file/directory list and create new row
		List<String> fDirList = getFileDirectoryList(currPath);
		TableRow newRow = null;
		TextView courseTxt = null;
		CheckBox courseCheck = null;
		if(fDirList.size()>0){
			LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
			for (int i = 0; i < fDirList.size(); i++) {
				String fName = fDirList.get(i);
				if (i % MAX_NUM_A_ROW == 0) {
					newRow = (TableRow)layoutInflater.inflate(R.layout.attrib_row, null);
					mainTable.addView(newRow);
				}
	
				if (i % MAX_NUM_A_ROW == 0) {
					courseTxt = (TextView) newRow.findViewById(R.id.record1);
					courseCheck = (CheckBox) newRow.findViewById(R.id.chk1);
				}
				else {
					courseTxt = (TextView) newRow.findViewById(R.id.record2);
					courseCheck = (CheckBox) newRow.findViewById(R.id.chk2);
				}
	
				String subFilePath = currPath + MyConstants.FILE_SEP + fName;
				if (new File(subFilePath).isFile()) {
					//Only show file name excluding extension
					courseTxt.setText(fName.substring(0, fName.lastIndexOf(".")));
					
					courseTxt.setTextAppearance(this.getApplicationContext(), R.style.File);
					courseTxt.setOnClickListener(fileClickListener);
					courseTxt.setOnLongClickListener(fileLongClickListener);
				}
				else {
					courseTxt.setText(fName);
					courseTxt.setTextAppearance(this.getApplicationContext(), R.style.Folder);
					courseTxt.setOnClickListener(folderEntryListener);
					courseTxt.setOnLongClickListener(folderLongClickListener);
				}
				
				courseCheck.setTag(fName);
				courseCheck.setOnCheckedChangeListener(checkedChangedListener);
				courseCheck.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v)
					{
						if(isMoveFiles){
							toastShortMessage(getString(R.string.msg_alert_reselect));							
							return;
						}			
					}
				});
				
				//If have selected, ticked the check box (files on moving)
				if(this.currSelectedList.contains(subFilePath)){
					courseCheck.setChecked(true);
				}
				//If isMoving, disable select
				if(this.isMoveFiles){
					courseCheck.setClickable(false);
				}
				
//				this.registerForContextMenu(courseTxt);
			}
			
			if(fDirList.size() % MAX_NUM_A_ROW != 0){
				courseTxt = (TextView) newRow.findViewById(R.id.record2);
				hideTextView(courseTxt);
				
				courseCheck = (CheckBox) newRow.findViewById(R.id.chk2);
				hideTextView(courseCheck);
			}
		}
		
//		this.enableMoving(this.isMoveFiles, false);
	}
	
	private void hideTextView(TextView v)
	{
		v.setTextSize(1);
		v.setVisibility(View.INVISIBLE);
		v.setEnabled(false);
	}
	
	/***
	 * If checked a file/folder, add to list; else, remove it from list
	 * @param selectedFile
	 * @param isChecked
	 */
	public void handleSelect(String selectedFile, boolean isChecked)
	{
		if(isChecked){
			currSelectedList.add(this.currPath + MyConstants.FILE_SEP + selectedFile);
		}
		else{
			currSelectedList.remove(selectedFile);
		}
		if(currSelectedList.size()>0 && !this.isMoveFiles){
			menuMove.setEnabled(true);
			menuDelete.setEnabled(true);
		}
		else{
			menuMove.setEnabled(false);
			menuDelete.setEnabled(false);
		}
	}

	/***
	 * Find file/foder list
	 * @param currPath
	 * @return
	 */
	private List<String> getFileDirectoryList(String currPath)
	{
		List<String> fDirList = new ArrayList<String>();

		// File rootDir = Environment.getRootDirectory();
		File rootDir = new File(currPath);
//		Log.d(MainActivity.class.toString(), "Root dir is " + rootDir);

		if (rootDir.isDirectory()) {
			fDirList.addAll(Arrays.asList(rootDir.list(jsmdFilter)));
		}

		return fDirList;
	}
	
	/***
	 * Find all of folders
	 * @param startRoot
	 * @return
	 */
	private List<String> findAllFolders(String startRoot)
	{
		List<String> fDirList = new ArrayList<String>();
		
		File directory = new File(startRoot);
		Collection<File> allDirs = FileUtils.listFilesAndDirs(directory, folderFilter, folderFilter);
		
		if(allDirs!=null && allDirs.size()>0){
			for(File f : allDirs){
				String path = StringUtils.replace(f.getPath(), MyConstants.FLASH_ROOT, "");
				if(StringUtils.isBlank(path)){
					path = MyConstants.FILE_SEP;
				}
				fDirList.add(path);
			}
		}
		
		return fDirList;
	}

	public String getCurrPath()
	{
		return currPath;
	}

	public void setCurrPath(String currPath)
	{
		this.currPath = currPath;
	}
	
	public String getTheLatestFilePath()
	{
		return theLatestFilePath;
	}

	public void setTheLatestFilePath(String theLatestFilePath)
	{
		this.theLatestFilePath = theLatestFilePath;
	}

	public String getParentPath()
	{
		File f = new File(getCurrPath());
		return f.getParent();
	}

	public List<String> getCurrSelectedList()
	{
		return currSelectedList;
	}

	/***
	 * Check if root flash path
	 * @return
	 */
	public boolean isRootPath()
	{
		return getCurrPath().equals(MyConstants.FLASH_ROOT);
	}
	
	public void enableBackButton(boolean isEnable)
	{
		btnBack.setEnabled(isEnable);
	}
	
	private void debug(String msg)
	{
		Log.d(MainActivity.class.getSimpleName(), msg);
	}
}
