/* ID: $Id: PlayerActivity.java 78 2013-10-07 08:30:17Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-10-07 16:30:17 +0800 (周一, 07 十月 2013) $
 * 
 * @version $Rev: 78 $
 */
package com.cnmpr.mprplayer;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.cnmpr.mprplayer.R;
import com.cnmpr.mprplayer.constant.MyConstants;
import com.cnmpr.mprplayer.core.BaseActivity;
import com.cnmpr.mprplayer.util.SharedPreferencesUtils;

import android.os.Bundle;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.util.Log;
import android.view.Menu;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PlayerActivity extends BaseActivity
{
	/* URL for Load html with file path parameter */
	private String PLAYER_URL = "file:///mnt/sdcard/sanmu/startplay.jsmp?file=";

	/* Base path of player page */
	private String PLAYER_BASE_URL = "file:///android_asset/html/";
	/* File name of player page */
	private String PLAYER_PAGE = "html/startplay.jsmp";
	/* Flag to replace file path */
	private static final String FLAG_FILE_NAME = "%fileName%";
	/* Byte object to store the content of player page */
	private byte[] playerData;

	private WebView mWebFlash = null;

	/* Current playing course file path */
	private String currFile;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// Clear title bar
		// requestWindowFeature(Window.FEATURE_NO_TITLE);

		// getWindow().setFlags(WindowManager.LayoutParams. FLAG_FULLSCREEN ,
		// WindowManager.LayoutParams. FLAG_FULLSCREEN);

		setContentView(R.layout.activity_player);

		// Read player content
		readPlayerContent();

		if (mWebFlash == null) {
			init();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_player, menu);
		return true;
	}

	public void init()
	{
		mWebFlash = (WebView) findViewById(R.id.web_flash);
		WebSettings settings = mWebFlash.getSettings();
		settings.setPluginState(WebSettings.PluginState.ON);
		settings.setJavaScriptEnabled(true);
		settings.setAllowFileAccess(true);
		settings.setDefaultTextEncodingName("GBK");
		
		settings.setLoadWithOverviewMode(true);
		settings.setUseWideViewPort(true);
		mWebFlash.setAnimationCacheEnabled(false);

		mWebFlash.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				view.loadUrl(url);
				return true;
			}
		});

		mWebFlash.setBackgroundColor(0);
	}

	private void readPlayerContent()
	{
		AssetManager assetManager = getAssets();
		InputStream input = null;
		try {
			input = assetManager.open(PLAYER_PAGE);
			playerData = new byte[input.available()];
			IOUtils.read(input, playerData);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadFlash()
	{
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String value = extras.getString(MyConstants.KEY_COURSE_FILE_PATH);
			if (value != null && value.trim().length() > 0) {
				currFile = value;
				Log.d(PlayerActivity.class.getName(), "Get file path from intent:" + currFile);
			}
		}
		
		this.init();
		mWebFlash.clearCache(true);

		String data = StringUtils.replace(new String(playerData), FLAG_FILE_NAME, currFile);
		
		mWebFlash.loadDataWithBaseURL(PLAYER_BASE_URL, data, "text/html", "utf-8", null);
		
		//Store the latest file
		recordLatestFile(currFile);
	}

	@Deprecated
	private void loadFlashByUrl()
	{

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String value = extras.getString(MyConstants.KEY_COURSE_FILE_PATH);
			if (value != null && value.trim().length() > 0) {
				currFile = value;
				Log.d(PlayerActivity.class.getName(), "Get file path from intent:" + currFile);

				/* Use relative path */
				String prefix = "flash/";
				int index = currFile.indexOf(prefix);
				currFile = currFile.substring(index);
				Log.d(PlayerActivity.class.getName(), "After cut, get file path:" + currFile);
			}
		}
		String url = PLAYER_URL + currFile;
		Log.d(PlayerActivity.class.toString(), "url is " + url);

		mWebFlash.loadUrl(url);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	public void onPause()
	{
		super.onPause();

		mWebFlash.loadData("", "text/html", "utf-8");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	public void onResume()
	{
		super.onResume();

		loadFlash();
	}

	private void recordLatestFile(String filePath)
	{
		SharedPreferencesUtils.writeData(this, MyConstants.KEY_OF_LATEST_FILE, filePath);
	}
}
