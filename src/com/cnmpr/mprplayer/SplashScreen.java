/* ID: $Id: MainActivity.java 63 2013-08-10 12:39:33Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-08-10 20:39:33 +0800 (周六, 10 八月 2013) $
 * 
 * @version $Rev: 63 $
 */
package com.cnmpr.mprplayer;

import com.cnmpr.mprplayer.core.BaseActivity;
import com.cnmpr.mprplayer.util.CourseValidator;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends BaseActivity
{

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 3000;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		if(!CourseValidator.checkActivation(this)){
			this.confirmExit(getString(R.string.title_alert_deactivated),getString(R.string.msg_alert_deactivated),false);			
		}
		else{
			new Handler().postDelayed(new Runnable() {
	
				/*
				 * Showing splash screen with a timer. This will be useful when you
				 * want to show case your app logo / company
				 */
	
				@Override
				public void run()
				{
					// This method will be executed once the timer is over
					// Start your app main activity
					Intent i = new Intent(SplashScreen.this, MainActivity.class);
					startActivity(i);
	
					// close this activity
					finish();
				}
			}, SPLASH_TIME_OUT);
		}
	}
}
