/* ID: $Id: FolderDirection.java 63 2013-08-10 12:39:33Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-08-10 20:39:33 +0800 (Sat, 10 Aug 2013) $
 * 
 * @version $Rev: 63 $
 */
package com.cnmpr.mprplayer.constant;

public enum FolderDirection {

	FORWARD(1),
	BACKWARD(0);
	
	private int code;
	
	private FolderDirection(int num){
		this.code = num;
	}

	public int getCode()
	{
		return code;
	}
	
}
