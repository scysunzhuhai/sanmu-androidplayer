/* ID: $Id: MyConstants.java 78 2013-10-07 08:30:17Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-10-07 16:30:17 +0800 (周一, 07 十月 2013) $
 * 
 * @version $Rev: 78 $
 */
package com.cnmpr.mprplayer.constant;

import java.io.File;

import android.os.Environment;

public class MyConstants
{
	
	/*Key for intent pass data: course file path*/
	public static final String KEY_COURSE_FILE_PATH = "KEY_COURSE_FILE_PATH";

	public static final String FILE_SEP = File.separator;
	
	/*Prefix for URL in local environment*/
	public static final String FILE_URL_PREFIX = "file://";
	
	/*Flash file path*/
	public static final String FLASH_ROOT = Environment.getExternalStorageDirectory() + "/sanmu/flash";
	
	/*The minimum size of file length for swf course*/
	public static final int MIN_FILE_SIZE = 1024 * 100;
	
	public static final String COURSE_FILE_EXTENSION = ".jsmd";
	public static final String COURSE_VALIDATION_FILE_EXTENSION = ".vld";
	public static final String COURSE_ORI_FILE_EXTENSION = ".swf";
	
	/*Desired preferences file name*/
	public final static String SANMU_PREFERENCE_FILE = "SanmuPreference";
	
	/* Key name of acticateKey in preference */
	public final static String KEY_OF_LATEST_FILE = "LatestFile";
}
