/* ID: $Id: BaseActivity.java 67 2013-08-14 15:12:19Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-08-14 23:12:19 +0800 (Wed, 14 Aug 2013) $
 * 
 * @version $Rev: 67 $
 */
package com.cnmpr.mprplayer.core;

import com.cnmpr.mprplayer.R;
import com.cnmpr.mprplayer.util.UIHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Toast;

public class BaseActivity extends Activity
{
	/***
	 * Show warning message by Toast
	 * @param msg
	 */
	public void toastWarningMessage(String msg)
	{
		Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_LONG).show();
	}
	
	/***
	 * Show common message in long period by Toast 
	 * @param msg
	 */
	public void toastLongMessage(String msg)
	{
		Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_LONG).show();
	}
	
	/***
	 * Show common message in short period by Toast 
	 * @param msg
	 */
	public void toastShortMessage(String msg)
	{
		Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_SHORT).show();
	}
	
	/***
	 * Create confirm exit dialog
	 * @param hasCancelButton
	 */
	public void confirmExit(boolean hasCancelButton)
	{
		this.confirmExit(getString(R.string.title_alert_exit), getString(R.string.msg_alert_exit), hasCancelButton);
	}
	
	/***
	 * Create confirm exit dialog
	 * @param title
	 * @param msg
	 * @param hasCancelButton
	 */
	public void confirmExit(String title, String msg, boolean hasCancelButton)
	{
		AlertDialog altDlgExit = this.createAlertDialog(
			title, msg,
			new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if(which == AlertDialog.BUTTON_POSITIVE){
						UIHelper.killApp(true);
					}
				}
			}, hasCancelButton
		);
	
		altDlgExit.show();
	}

	
	/***
	 * Create alertDialog for confirm
	 * @param msg
	 * @return
	 */
	public AlertDialog createAlertDialog(String title, String msg, DialogInterface.OnClickListener listener)
	{
		return this.createAlertDialog(title, msg, listener, true);
	}
	
	/***
	 * Create alertDialog for confirm
	 * @param msg
	 * @return
	 */
	public AlertDialog createAlertDialog(String title, String msg, DialogInterface.OnClickListener listener, boolean hasCancel)
	{
		AlertDialog alertDialog = null;
		
		AlertDialog.Builder altDlgBuilder = new AlertDialog.Builder(this);
		alertDialog = altDlgBuilder.create();
		
		alertDialog.setTitle(title);
		alertDialog.setMessage(msg);
		
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.lbl_ok), listener);
		if(hasCancel){
			alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.lbl_cancel), listener);
		}
		
		return alertDialog;
	}
}
