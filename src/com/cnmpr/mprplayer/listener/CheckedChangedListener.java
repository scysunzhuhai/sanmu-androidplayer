/* ID: $Id: CheckedChangedListener.java 63 2013-08-10 12:39:33Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-08-10 20:39:33 +0800 (Sat, 10 Aug 2013) $
 * 
 * @version $Rev: 63 $
 */
package com.cnmpr.mprplayer.listener;

import com.cnmpr.mprplayer.MainActivity;

import android.util.Log;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class CheckedChangedListener implements OnCheckedChangeListener
{

	private MainActivity mainActivity;

	/***
	 * 
	 * @param activity
	 */
	public CheckedChangedListener(MainActivity activity)
	{
		super();
		mainActivity = activity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.CompoundButton.OnCheckedChangeListener#onCheckedChanged
	 * (android.widget.CompoundButton, boolean)
	 */
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
		String fName = (String) buttonView.getTag();
//		Log.d(CheckedChangedListener.class.getName(), "File " + fName + " is checked ? " + isChecked);
		mainActivity.handleSelect(fName, isChecked);
	}

}
