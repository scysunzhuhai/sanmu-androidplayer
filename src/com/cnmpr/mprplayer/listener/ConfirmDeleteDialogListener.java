/* ID: $Id: UniversalDaoImpl.java 4384 2013-07-17 02:57:54Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-07-17 10:57:54 +0800 (周三, 17 七月 2013) $
 * 
 * @version $Rev: 4384 $
 */
package com.cnmpr.mprplayer.listener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.cnmpr.mprplayer.MainActivity;
import com.cnmpr.mprplayer.R;
import com.cnmpr.mprplayer.constant.MyConstants;

import android.app.AlertDialog;
import android.content.DialogInterface;

public class ConfirmDeleteDialogListener implements DialogInterface.OnClickListener
{
	private MainActivity mainActivity;
	
	public ConfirmDeleteDialogListener(MainActivity activity){
		super();
		mainActivity = activity;
	}

	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		if(which == AlertDialog.BUTTON_POSITIVE){
			List<String> currSelectedList = mainActivity.getCurrSelectedList();
			
			//Toast.makeText(MainActivity.this, "Prepare delete files ...", Toast.LENGTH_SHORT).show();
			StringBuffer deleteMsg = new StringBuffer(mainActivity.getString(R.string.msg_after_delete_msg1)).append(IOUtils.LINE_SEPARATOR);
			List<String> deletedList = new ArrayList<String>();
			for(int i=0;i<currSelectedList.size();i++){
				String selected = currSelectedList.get(i);
				try {
					File f = new File(selected);
					if(f.isDirectory()){
						FileUtils.deleteDirectory(f);
					}
					else{
						f.delete();
					}
					deletedList.add(selected);
					
					String fNameWithoutExt = StringUtils.replace(f.getName(), MyConstants.COURSE_FILE_EXTENSION, "");
					if(i<MainActivity.MAX_NUM_OF_FILE_HISTORY)
						deleteMsg.append(fNameWithoutExt).append(IOUtils.LINE_SEPARATOR);
				} catch (IOException e) {
					e.printStackTrace();
					mainActivity.toastWarningMessage(mainActivity.getString(R.string.lbl_delete) + "[" + selected + "]" + mainActivity.getString(R.string.lbl_failed));
					return;
				}
			}
			if(MainActivity.MAX_NUM_OF_FILE_HISTORY < deletedList.size()){
				deleteMsg.append("......");
			}
			mainActivity.toastLongMessage(deleteMsg.toString());
			
			currSelectedList.removeAll(deletedList);
			
			mainActivity.refreshDiretory();
		}
		else{
			dialog.cancel();
		}
		
	}

}
