/* ID: $Id: UniversalDaoImpl.java 4384 2013-07-17 02:57:54Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-07-17 10:57:54 +0800 (周三, 17 七月 2013) $
 * 
 * @version $Rev: 4384 $
 */
package com.cnmpr.mprplayer.listener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.cnmpr.mprplayer.MainActivity;
import com.cnmpr.mprplayer.R;
import com.cnmpr.mprplayer.constant.MyConstants;

import android.app.AlertDialog;
import android.content.DialogInterface;

/***
 * 
 * @author phoenix
 *
 */
public class ConfirmMoveDialogListener implements DialogInterface.OnClickListener
{
	private MainActivity mainActivity;
	
	public ConfirmMoveDialogListener(MainActivity activity){
		super();
		mainActivity = activity;
	}

	/*
	 * (non-Javadoc)
	 * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
	 */
	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		if(which == AlertDialog.BUTTON_POSITIVE){
			List<String> currSelectedList = mainActivity.getCurrSelectedList();
			
			StringBuffer movedMsg = new StringBuffer(mainActivity.getString(R.string.msg_after_move_msg1)).append(IOUtils.LINE_SEPARATOR);
			List<String> moveddList = new ArrayList<String>();
			for(int i=0;i<currSelectedList.size();i++){
				String selected = currSelectedList.get(i);
				try {
					File srcFile = new File(selected);
					File destFile = new File(mainActivity.getCurrPath() + MyConstants.FILE_SEP + srcFile.getName());
					if(srcFile.isDirectory()){
						FileUtils.moveDirectory(srcFile, destFile);
					}
					else{
						FileUtils.moveFile(srcFile, destFile);
					}
					moveddList.add(selected);
					
					String fNameWithoutExt = StringUtils.replace(srcFile.getName(), MyConstants.COURSE_FILE_EXTENSION, "");
					if(i<MainActivity.MAX_NUM_OF_FILE_HISTORY)
						movedMsg.append(fNameWithoutExt).append(IOUtils.LINE_SEPARATOR);
				} catch (IOException e) {
					e.printStackTrace();
					mainActivity.toastWarningMessage(mainActivity.getString(R.string.lbl_move) + "[" + selected + "]" + mainActivity.getString(R.string.lbl_failed));
					return;
				}
			}
			if(MainActivity.MAX_NUM_OF_FILE_HISTORY < moveddList.size()){
				movedMsg.append("......");
			}
			mainActivity.toastLongMessage(movedMsg.toString());
			
			currSelectedList.removeAll(moveddList);
			
			mainActivity.enableMoving(false, true);
		}
		else{
			dialog.cancel();
		}
		
	}

}
