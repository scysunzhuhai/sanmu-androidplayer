/* ID: $Id: ConfirmPlayLatestDialogListener.java 79 2013-10-07 14:00:39Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-10-07 22:00:39 +0800 (周一, 07 十月 2013) $
 * 
 * @version $Rev: 79 $
 */
package com.cnmpr.mprplayer.listener;

import java.io.File;
import org.apache.commons.lang3.StringUtils;

import com.cnmpr.mprplayer.MainActivity;
import com.cnmpr.mprplayer.constant.FolderDirection;
import com.cnmpr.mprplayer.constant.MyConstants;
import com.cnmpr.mprplayer.util.PlayerUtils;
import com.cnmpr.mprplayer.util.SharedPreferencesUtils;

import android.app.AlertDialog;
import android.content.DialogInterface;

/***
 * 
 * @author phoenix
 *
 */
public class ConfirmPlayLatestDialogListener implements DialogInterface.OnClickListener
{
	private MainActivity mainActivity;
	
	public ConfirmPlayLatestDialogListener(MainActivity activity){
		super();
		mainActivity = activity;
	}

	/*
	 * (non-Javadoc)
	 * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
	 */
	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		if(which == AlertDialog.BUTTON_POSITIVE){
			String currFile = mainActivity.getTheLatestFilePath();
			if(StringUtils.isNotBlank(currFile)){
				File currFileObj = new File(currFile);
				if(currFileObj.exists()){
					String currFolder = currFileObj.getParent(); 
					mainActivity.updateCurrentPath(currFolder, FolderDirection.BACKWARD);
					mainActivity.refreshDiretory();
					
					PlayerUtils.callPlayerActivity(mainActivity, currFile);
				}
			}
		}
		else{
			dialog.cancel();
			
			//Clear the latest palyer file
			String theLatestFilePath = null;
			mainActivity.setTheLatestFilePath(theLatestFilePath);
			SharedPreferencesUtils.writeData(mainActivity, MyConstants.KEY_OF_LATEST_FILE, theLatestFilePath);
		}
		
	}

}
