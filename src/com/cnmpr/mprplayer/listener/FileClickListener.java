/* ID: $Id: FileClickListener.java 78 2013-10-07 08:30:17Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-10-07 16:30:17 +0800 (周一, 07 十月 2013) $
 * 
 * @version $Rev: 78 $
 */
package com.cnmpr.mprplayer.listener;

import com.cnmpr.mprplayer.MainActivity;
import com.cnmpr.mprplayer.PlayerActivity;
import com.cnmpr.mprplayer.constant.MyConstants;
import com.cnmpr.mprplayer.util.CourseValidator;
import com.cnmpr.mprplayer.util.PlayerUtils;
import com.cnmpr.mprplayer.util.UIHelper;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

/***
 * 点击播放课件事件响应
 * @author phoenix
 *
 */
public class FileClickListener implements OnClickListener
{

	private MainActivity mainActivity;

	public FileClickListener(MainActivity activity)
	{
		super();
		mainActivity = activity;
	}

	public void onClick(View v)
	{

		String fileName = "";
		if (v instanceof TextView) {
			fileName = ((TextView) v).getText().toString();
		}
		String currFile = mainActivity.getCurrPath() + MyConstants.FILE_SEP + fileName
				+ MyConstants.COURSE_FILE_EXTENSION;
		String vldFile = currFile + MyConstants.COURSE_VALIDATION_FILE_EXTENSION;
		boolean isValid = CourseValidator.validateCourse(mainActivity, currFile, vldFile);
		if(isValid){
			PlayerUtils.callPlayerActivity(mainActivity, currFile);
		}
		else{
			AlertDialog dlg = mainActivity.createAlertDialog("未授权", "您未授权播放该课件，请与出版社联系！", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					//do nothing
				}
			}, false);
			
			dlg.show();
		}
	}

}
