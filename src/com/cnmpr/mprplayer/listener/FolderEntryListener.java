/* ID: $Id: FolderEntryListener.java 63 2013-08-10 12:39:33Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-08-10 20:39:33 +0800 (Sat, 10 Aug 2013) $
 * 
 * @version $Rev: 63 $
 */
package com.cnmpr.mprplayer.listener;

import com.cnmpr.mprplayer.MainActivity;
import com.cnmpr.mprplayer.constant.FolderDirection;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class FolderEntryListener implements OnClickListener
{

	private MainActivity mainActivity;

	public FolderEntryListener(MainActivity activity)
	{
		super();
		mainActivity = activity;
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	public void onClick(View v)
	{
		if (v instanceof Button) {
			// Back to parent folder
//			if (((Button) v).getText().toString().equals(R.string.lbl_button_back)) {
				mainActivity.updateCurrentPath(mainActivity.getParentPath(), FolderDirection.BACKWARD);
				mainActivity.refreshDiretory();
//			}
		}
		else if (v instanceof TextView) {
			mainActivity.updateCurrentPath(((TextView) v).getText().toString(), FolderDirection.FORWARD);

			mainActivity.refreshDiretory();
		}
	}

}
