/* ID: $Id: LongClickListener.java 63 2013-08-10 12:39:33Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-08-10 20:39:33 +0800 (Sat, 10 Aug 2013) $
 * 
 * @version $Rev: 63 $
 */
package com.cnmpr.mprplayer.listener;

import com.cnmpr.mprplayer.MainActivity;
import com.cnmpr.mprplayer.PlayerActivity;
import com.cnmpr.mprplayer.constant.MyConstants;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.TextView;

public class LongClickListener implements OnLongClickListener {

	private MainActivity mainActivity;
	private boolean isFileClick;
	
	public LongClickListener(MainActivity activity, boolean isFileClick){
		super();
		this.mainActivity = activity;
		this.isFileClick = isFileClick;
	}
	
	public void onClick(View v) {
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
		
		intent.setClassName("com.cnmpr.mprplayer", PlayerActivity.class.getCanonicalName());
		
		String fileName = "";
		if(v instanceof TextView){
			fileName = ((TextView)v).getText().toString();
		}
		String currFile = mainActivity.getCurrPath() + MyConstants.FILE_SEP + fileName + MyConstants.COURSE_FILE_EXTENSION;
		intent.putExtra(MyConstants.KEY_COURSE_FILE_PATH, currFile);
		
		mainActivity.startActivity(intent);
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnLongClickListener#onLongClick(android.view.View)
	 */
	public boolean onLongClick(View v) {
		String fileName = "";
		if(v instanceof TextView){
			fileName = ((TextView)v).getText().toString();
		}
		
		String currFile = mainActivity.getCurrPath() + MyConstants.FILE_SEP + fileName;
		if(this.isFileClick){
			currFile = currFile + MyConstants.COURSE_FILE_EXTENSION;
		}
		
		//mainActivity.
		Log.d(LongClickListener.class.getName(), "Long click file:" + currFile);
		
		return true;
	}

}
