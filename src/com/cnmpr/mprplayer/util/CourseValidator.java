/* ID: $Id: MainActivity.java 65 2013-08-13 23:52:59Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-08-14 07:52:59 +0800 (周三, 14 八月 2013) $
 * 
 * @version $Rev: 65 $
 */
package com.cnmpr.mprplayer.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.cnmpr.mprplayer.constant.MyConstants;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;


public class CourseValidator
{

	private final static String ACTIVATE_KEY_FILE = "activateKey.dat";
	
	/*Key name of acticateKey in preference*/
	private final static String KEY_OF_ACTIVATE_KEY = "YourKey";

	/***
	 * Validate a course file
	 * @param activity
	 * @param currFile 
	 * Full file path of course file
	 * @param vldFile
	 * Full file path of validation file
	 * @return
	 */
	public static boolean validateCourse(Activity activity, String currFile, String vldFile)
	{
		boolean result = false;
		String activateKey = getActivationKey(activity);
		if(StringUtils.isBlank(activateKey)){
			return false;
		}
		try {
			String courseName = new File(currFile).getName();
			String srcVldValue = MyDigestUtils.md5Hex(courseName + activateKey);
			String targetVldContent = FileUtils.readFileToString(new File(vldFile));
			return srcVldValue.equals(targetVldContent);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/***
	 * Check activated or not
	 * @param activity
	 * @return
	 */
	public static boolean checkActivation(Activity activity)
	{
		boolean result = true;
		
		String yourKey = getActivationKey(activity);
		
		result = StringUtils.isNotBlank(yourKey);
		
		return result;
	}
	
	/***
	 * Read the getActivationKey value
	 * @param activity
	 * @return
	 */
	public static String getActivationKey(Activity activity)
	{
		SharedPreferences pref = activity.getSharedPreferences(MyConstants.SANMU_PREFERENCE_FILE, Context.MODE_WORLD_WRITEABLE);
		String yourKey = pref.getString(KEY_OF_ACTIVATE_KEY, null);

		//强制检查key文件是否存在，如存在，则读取内容并删除
		//if(StringUtils.isBlank(yourKey)){
			//read from key file
			File keyFile = new File(MyConstants.FLASH_ROOT + MyConstants.FILE_SEP + ACTIVATE_KEY_FILE);
			if(keyFile.exists()){
				try {
					yourKey = FileUtils.readFileToString(keyFile);
					SharedPreferences.Editor editor = pref.edit();
					editor.putString(KEY_OF_ACTIVATE_KEY, yourKey);
			        editor.commit();
					//Delete the key file
					keyFile.delete();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		//}
		
		return yourKey;
	}
}
