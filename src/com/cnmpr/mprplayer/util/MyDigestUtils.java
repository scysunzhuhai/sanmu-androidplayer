package com.cnmpr.mprplayer.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MyDigestUtils
{
	public static String md5Hex(String input)
	{

		MessageDigest md = null;

		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}

		md.update(input.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

//		System.out.println("Digest(in hex format):: " + sb.toString());

		// convert the byte to hex format method 2
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}
		return hexString.toString();
	}
	
	public static void main(String[] args)
	{
		String fileName = "1";
		String regString = "3EE72DBD9250B1B76E82987EE4B6F3F0EA7C240DE1F3D5F899D5F0542495463966FD29B48381775394688C511283ACD22056C18DCB93CDB26B1145B79182486E";
		System.out.println(md5Hex(fileName + regString));
		
	}
}
