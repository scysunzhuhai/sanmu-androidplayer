/* ID: $Id: MyFilenameFilter.java 63 2013-08-10 12:39:33Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-08-10 20:39:33 +0800 (Sat, 10 Aug 2013) $
 * 
 * @version $Rev: 63 $
 */
package com.cnmpr.mprplayer.util;

import java.io.File;
import java.io.FilenameFilter;

import com.cnmpr.mprplayer.constant.MyConstants;

public class MyFilenameFilter implements FilenameFilter
{
	public boolean accept(File dir, String filename)
	{
		File testFile = new File(dir.getPath() + MyConstants.FILE_SEP + filename);
		if(testFile.isDirectory()){
			return true;
		}
		else{
			//FIXME For test, rename the swf to jsmd
			if(filename.toLowerCase().endsWith(MyConstants.COURSE_ORI_FILE_EXTENSION)){
				int index = filename.lastIndexOf(".");
				String encriptFileName = filename.substring(0, index) + MyConstants.COURSE_FILE_EXTENSION;
				
				File encriptFile = new File(dir.getPath() + MyConstants.FILE_SEP + encriptFileName);
				if(!encriptFile.exists()){
					testFile.renameTo(encriptFile);
				}
			}
		}
		
		//Test file size
		if(testFile.length() < MyConstants.MIN_FILE_SIZE){
			return false;
		}
		//Test file extension
		return filename.toLowerCase().endsWith(MyConstants.COURSE_FILE_EXTENSION);
	}

}
