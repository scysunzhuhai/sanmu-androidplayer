/* ID: $Id: UniversalDaoImpl.java 4384 2013-07-17 02:57:54Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-07-17 10:57:54 +0800 (周三, 17 七月 2013) $
 * 
 * @version $Rev: 4384 $
 */
package com.cnmpr.mprplayer.util;

import java.io.File;

import org.apache.commons.io.filefilter.IOFileFilter;

import com.cnmpr.mprplayer.constant.MyConstants;

public class MyFolderFilter implements IOFileFilter
{
	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.io.filefilter.IOFileFilter#accept(java.io.File, java.lang.String)
	 */
	@Override
	public boolean accept(File dir, String filename)
	{
		File testFile = new File(dir.getPath() + MyConstants.FILE_SEP + filename);
		
		return checkIfDirectory(testFile);
				
		/*
		if(testFile.isDirectory()){
			return true;
		}
		else{
			return false;
		}
		*/
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.io.filefilter.IOFileFilter#accept(java.io.File)
	 */
	@Override
	public boolean accept(File file)
	{
		//Always false to ignore files
		return checkIfDirectory(file);
	}
	
	private boolean checkIfDirectory(File f)
	{
		if(f.isDirectory()){
			return true;
		}
		else{
			return false;
		}
	}
}
