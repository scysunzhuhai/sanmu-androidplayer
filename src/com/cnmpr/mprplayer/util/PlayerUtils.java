/* ID: $Id: PlayerUtils.java 78 2013-10-07 08:30:17Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-10-07 16:30:17 +0800 (周一, 07 十月 2013) $
 * 
 * @version $Rev: 78 $
 */
package com.cnmpr.mprplayer.util;

import android.app.Activity;
import android.content.Intent;

import com.cnmpr.mprplayer.PlayerActivity;
import com.cnmpr.mprplayer.constant.MyConstants;

public class PlayerUtils
{
	/***
	 * Call PlayerActivity
	 * @param mainActivity
	 * @param currFile
	 */
	public static void callPlayerActivity(Activity mainActivity, String currFile)
	{
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);

		intent.setClassName("com.cnmpr.mprplayer", PlayerActivity.class.getCanonicalName());

		intent.putExtra(MyConstants.KEY_COURSE_FILE_PATH, currFile);

		mainActivity.startActivity(intent);
	}
}
