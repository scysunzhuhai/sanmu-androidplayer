/* ID: $Id: SharedPreferencesUtils.java 78 2013-10-07 08:30:17Z phoenix $ */

/**
 * 
 * @author $Author: phoenix $
 * 
 * @lastrevision $Date: 2013-10-07 16:30:17 +0800 (周一, 07 十月 2013) $
 * 
 * @version $Rev: 78 $
 */
package com.cnmpr.mprplayer.util;

import org.apache.commons.lang3.StringUtils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.cnmpr.mprplayer.constant.MyConstants;

public class SharedPreferencesUtils
{
	/***
	 * Write data into SharedPreferences
	 * @param activity
	 * @param key
	 * @param value
	 */
	public static void writeData(Activity activity, String key, String value)
	{
		SharedPreferences pref = activity.getSharedPreferences(MyConstants.SANMU_PREFERENCE_FILE,
			Context.MODE_WORLD_WRITEABLE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	/***
	 * Read data from SharedPreferences
	 * @param activity
	 * @param key
	 * @return
	 */
	public static String readData(Activity activity, String key)
	{
		return readData(activity, key, null);
	}
	
	/***
	 * Read data from SharedPreferences
	 * @param activity
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static String readData(Activity activity, String key, String defValue)
	{
		SharedPreferences pref = activity.getSharedPreferences(MyConstants.SANMU_PREFERENCE_FILE,
			Context.MODE_WORLD_READABLE);
		String result = pref.getString(key, null);
		if(StringUtils.isBlank(result)){
			return defValue;
		}
		return result;
	}
}
